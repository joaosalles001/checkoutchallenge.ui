import axios from "axios";

const baseUrl = "http://localhost:5000/api/client/";

const loginClient = clientName => {
  return axios
    .post(baseUrl + "Login", { value: clientName })
    .then(result => result.data);
};

module.exports = {
  loginClient: clientName => loginClient(clientName)
};
 