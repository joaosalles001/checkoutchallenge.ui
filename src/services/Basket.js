import axios from "axios";

const baseUrl = "http://localhost:5000/api/basket/";

const fetchProductList = clientId => {
  return axios
    .post(baseUrl + "GetByClientId", {
      value: clientId
    })
    .then(result => result.data);
};

const addUpdateProduct = (clientId, productId, quantity) => {
  const payload = {
    clientId: clientId,
    productId: productId,
    quantity: quantity
  };

  return axios
    .post(baseUrl + "AddUpdateProduct", payload)
    .then(result => result.data);
};

const removeProduct = (clientId, productId) => {
  const payload = {
    clientId: clientId,
    productId: productId
  };

  return axios
    .post(baseUrl + "RemoveProduct", payload)
    .then(result => result.data);
};

const emptyBasket = clientId => {
  const payload = {
    value: clientId
  };

  return axios
    .post(baseUrl + "EmptyBasket", payload)
    .then(result => result.data);
};

module.exports = {
  fetchProductList: clientName => fetchProductList(clientName),
  addUpdateProduct: (clientId, productId, quantity) =>
    addUpdateProduct(clientId, productId, quantity),
  removeProduct: (clientId, productId) => removeProduct(clientId, productId),
  emptyBasket: clientId => emptyBasket(clientId)
};
