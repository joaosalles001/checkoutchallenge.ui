import axios from "axios";

const baseUrl = "http://localhost:5000/api/product/";

const getDetailsByCode = productCode => {
  return axios
    .post(baseUrl + "GetByCode", { value: productCode })
    .then(result => result.data);
};

const listProducts = () => {
  return axios
    .post(baseUrl + "List")
    .then(result => result.data);
};

module.exports = {
  getDetailsByCode: productCode => getDetailsByCode(productCode),
  listProducts: () => listProducts()
};
 