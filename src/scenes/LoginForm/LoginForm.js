import React, { Component } from "react";
import TextInput from "~/components/TextInput/TextInput";

import { loginClient } from "~/services/Client";

class LoginForm extends Component {
  loginClient = clientName => {
    const { handleLogin } = this.props;

    loginClient(clientName).then(result => {
      if (result.error === false) {
        handleLogin(result.data);
      }
    });
  };
  render() {
    return (
      <TextInput
        placeholder="Enter your username here"
        label="Username"
        buttonLabel="Login"
        handleClick={this.loginClient}
      />
    );
  }
}

export default LoginForm;
