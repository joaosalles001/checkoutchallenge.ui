import React, { Component } from "react";
import { Dropdown, Menu, Input, Button } from "semantic-ui-react";

import { listProducts } from "~/services/Product";

class AddItemForm extends Component {
  state = {
    productList: [],
    selectedItem: 0,
    quantity: 0
  };
  componentDidMount() {
    listProducts().then(result => {
      this.setState({
        productList: result.data
      });
    });
  }
  productOptions = productList => {
    return productList.map(product => {
      return {
        key: product.id,
        value: product.id,
        text: product.description
      };
    });
  };
  handleSelectionChange = (event, data) => {
    this.setState({
      selectedItem: data.value
    });
  };
  handleQuantityUpdate = (event, data) => {
    if (!isNaN(data.value)) {
      this.setState({
        quantity: data.value
      });
    }
  };
  handleInsert = () => {
    const { selectedItem, quantity } = this.state;
    const { insertItem } = this.props;

    if (selectedItem > 0 && quantity > 0) {
      insertItem(selectedItem, quantity);
    }
  }
  render() {
    const { productList } = this.state;

    return (
      <div>
        <Menu compact>
          <Dropdown
            placeholder="Select your item"
            fluid
            selection
            options={this.productOptions(productList)}
            onChange={this.handleSelectionChange}
          />
        </Menu>
        <Input placeholder="Quantity" onChange={this.handleQuantityUpdate} />
        <Button onClick={this.handleInsert}>Insert!</Button>
      </div>
    );
  }
}

export default AddItemForm;
