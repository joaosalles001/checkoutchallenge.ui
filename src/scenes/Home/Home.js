import React, { Component } from "react";
import { loginClient } from "~/services/Client";

import LoginForm from "~/scenes/LoginForm/LoginForm";
import Basket from "~/scenes/Basket/Basket";

class Home extends Component {
  state = {
    client: { Id: 0, Name: "" }
  };
  handleLogin = clientData => {
    this.setState({
      client: {
        Id: clientData.id,
        Name: clientData.name
      }
    });
  };
  render() {
    const { Id, Name } = this.state.client;
    if (Id === 0) {
      return <LoginForm handleLogin={this.handleLogin} />;
    } else {
      return (
        <div>
          <p>Hello, {Name}. Here is your shopping basket.</p>
          {Id > 0 && <Basket clientId={Id} />}
        </div>
      );
    }
  }
}

export default Home;
