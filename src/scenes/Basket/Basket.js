import React, { Component } from "react";
import { Button, Divider } from "semantic-ui-react";
import BasketGrid from "~/components/BasketGrid/BasketGrid";
import AddItemForm from "~/scenes/AddItemForm/AddItemForm";

import {
  fetchProductList,
  addUpdateProduct,
  removeProduct,
  emptyBasket
} from "~/services/Basket";

class Basket extends Component {
  state = {
    productList: [],
    loading: true
  };
  componentDidMount() {
    const { clientId } = this.props;
    this.fetchProductList(clientId);
  }
  fetchProductList = clientId => {
    fetchProductList(clientId).then(result => {
      this.setState({
        productList: result.data.productList,
        loading: false
      });
    });
  };
  addProduct = (productId, quantity) => {
    const { clientId } = this.props;

    addUpdateProduct(clientId, productId, quantity).then(result => {
      if (result.error === false) {
        this.fetchProductList(clientId);
      }
    });
  };
  updateProductQuantity = (productId, newQuantity) => {
    const { clientId } = this.props;
    if (newQuantity > 0) {
      addUpdateProduct(clientId, productId, newQuantity).then(result => {
        if (result.error === false) {
          this.fetchProductList(clientId);
        }
      });
    } else {
      removeProduct(clientId, productId).then(result => {
        if (result.error === false) {
          this.fetchProductList(clientId);
        }
      });
    }
  };
  emptyBasket = _ => {
    const { clientId } = this.props;

    emptyBasket(clientId).then(result => {
      if (result.error === false) {
        this.fetchProductList(clientId);
      }
    });
  };
  render() {
    const { loading, productList } = this.state;

    if (loading) {
      return <div>Loading...</div>;
    } else if (productList.length == 0) {
      return (
        <div>
          <AddItemForm insertItem={this.addProduct} />
          <Divider />
          <div>You have no items in your basket</div>
        </div>
      );
    } else {
      return (
        <div>
          <AddItemForm insertItem={this.addProduct} />
          <BasketGrid
            data={productList}
            updateQuantity={this.updateProductQuantity}
          />
          <Button onClick={this.emptyBasket}>Empty Basket</Button>
        </div>
      );
    }
  }
}

export default Basket;
