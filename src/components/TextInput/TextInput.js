import React, { Component } from "react";
import { Input, Button } from "semantic-ui-react";

class TextInput extends Component {
  state = {
    userName: ""
  };
  handleClick = _ => {
    const { handleClick } = this.props;
    const { userName } = this.state;

    if (userName != "") {
      handleClick(userName);
    }
  };
  handleChange = (event, data) => {
    this.setState({
      userName: data.value
    });
  };
  render() {
    const { label, placeholder, buttonLabel } = this.props;
    return (
      <Input
        label={label}
        action={<Button content={buttonLabel} onClick={this.handleClick} />}
        placeholder={placeholder}
        onChange={this.handleChange}
      />
    );
  }
}

export default TextInput;
