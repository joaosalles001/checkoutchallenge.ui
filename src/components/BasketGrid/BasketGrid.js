import React, { Component } from "react";
import { Table, Button, Icon } from "semantic-ui-react";

class BasketGrid extends Component {
  handleUpdateQuantity = (productId, newQuantity) => {
    const { updateQuantity } = this.props;

    updateQuantity(productId, newQuantity);
  };
  render() {
    const { data } = this.props;

    return (
      <Table celled compact selectable>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>Item</Table.HeaderCell>
            <Table.HeaderCell>Unit Price</Table.HeaderCell>
            <Table.HeaderCell>Quantity</Table.HeaderCell>
            <Table.HeaderCell>Subtotal</Table.HeaderCell>
            <Table.HeaderCell />
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {data.map((item, index) => {
            return (
              <Table.Row key={index}>
                <Table.Cell>{item["description"]}</Table.Cell>
                <Table.Cell>${item["unitValue"].toFixed(2)}</Table.Cell>
                <Table.Cell>
                  <Button
                    circular
                    icon="minus"
                    onClick={() =>
                      this.handleUpdateQuantity(item.id, item.quantity - 1)
                    }
                  />
                  {item["quantity"]}
                  <Button
                    circular
                    icon="plus"
                    onClick={() =>
                      this.handleUpdateQuantity(item.id, item.quantity + 1)
                    }
                  />
                </Table.Cell>
                <Table.Cell>
                  ${(item["unitValue"] * item["quantity"]).toFixed(2)}
                </Table.Cell>
                <Table.Cell>
                  <Button
                    circular
                    icon="trash"
                    onClick={() => this.handleUpdateQuantity(item.id, 0)}
                  />
                </Table.Cell>
              </Table.Row>
            );
          })}
        </Table.Body>
      </Table>
    );
  }
}

export default BasketGrid;
