# THE CHALLENGE

## Part 2:
Create a client library that makes use of the API endpoints created in Part 1.  The purpose of this code to provide authors of client applications a simple framework to use in their applications.

# RUNNING INSTRUCTIONS

Start the application with

```
npm start
```

after starting the backend and exposing the port 5000.

# IMPORTANT DETAILS

It was not necessary to build a React App for this challenge, but hey! it's a lot more fun than just writing a boring Flurl application.

The "client library" mentioned by the challenge is in the services folder, and could be delivered to end customers as a package.

The rest was just a bored developer having some fun! :D